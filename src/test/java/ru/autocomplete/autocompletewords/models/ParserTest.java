package ru.autocomplete.autocompletewords.models;


import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import ru.autocomplete.autocompletewords.services.Parser;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

/**
 * Created by General on 5/14/2017.
 */
@RunWith(org.junit.platform.runner.JUnitPlatform.class)
public class ParserTest {
    private Parser parser;
    private RatingDictionary dictionary;
    private ArrayList<String> prefixes;

    private String createStdinByArray(int wordCount, String[] words, int[] reting,
                                            int prefixCount, String[] prefixes){
        int maxLength = Math.max(words.length, reting.length);
        StringBuilder result = new StringBuilder();
        result.append(wordCount).append("\n");
        for (int i = 0; i < maxLength; i++) {
            if (i < words.length)
                result.append(words[i]);
            result.append(" ");
            if (i < reting.length)
                result.append(reting[i]);
            result.append("\n");
        }

        result.append(prefixCount).append("\n");
        for (String prefix:
                prefixes) {
            result.append(prefix).append("\n");
        }
        return result.toString();
    }


    @BeforeEach
    public void setUp() {
        dictionary = new RatingDictionary();
        prefixes = new ArrayList<>();
    }

    @AfterEach
    public void tearDown() {
        parser = null;
    }

    @Test
    public void parseRunWithCorrectStream() {
        //arrange
        final String[] expectedWords = new String[]{"hello", "hi", "bye"};
        final int[] excpectedRetings = new int[]{5, 10, 10};
        final String[] expectedPrefix = new String[]{"h", "he"};
        String stdin = createStdinByArray(expectedWords.length, expectedWords,
                excpectedRetings, expectedPrefix.length, expectedPrefix);

        try(ByteArrayInputStream std = new ByteArrayInputStream(stdin.getBytes())){
            parser = new Parser(dictionary, prefixes, std);

            //act
            parser.run();

            //assert

            assertEquals(expectedWords.length, dictionary.getWordCount(), "The number " +
                    "of words in the input stream does not match the number of words in the dictionary");

            assertEquals(expectedPrefix.length, prefixes.size(), "The number of prefixes " +
                    "in the input stream does not match the number of prefix after analysis");

            HashMap<String, Integer> resultDictionary = dictionary.getHashDictionary();
            for (int i = 0; i < expectedWords.length; i++) {
                assertEquals(Integer.valueOf(excpectedRetings[i]),
                        resultDictionary.get(expectedWords[i]),
                        "Words or number of repetitions in the text do not coincide " +
                                "with the standard");
            }

            for (int i = 0; i < expectedPrefix.length; i++) {
                assertEquals(expectedPrefix[i], prefixes.get(i));
            }

        } catch (IOException e) {
            fail("Error in creating the input stream to the test: " + e.getMessage());
        }


    }

}