package ru.autocomplete.autocompletewords.services;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import ru.autocomplete.autocompletewords.models.RatingDictionary;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * Simple test AutocompleteWords
 */
@RunWith(org.junit.platform.runner.JUnitPlatform.class)
public class AutocompleteWordsExecutorTest {
    private AutocompleteWordsExecutor executor;
    private RatingDictionary dictionary;
    private ArrayList<String> prefixes;
    private ArrayList<List<String>> results;

    private ArrayList<String> createPrefixesByArray(String[] prefixes){
        ArrayList<String> result = new ArrayList<>();
        for (String prefix :
                prefixes) {
            result.add(prefix);
        }
        return result;
    }

    private RatingDictionary createDictionaryByArray(String[] words, int[] reting){
        RatingDictionary dictionary = new RatingDictionary();
        int maxLength = Math.max(words.length, reting.length);
        for (int i = 0; i < maxLength; i++) {
            dictionary.addWord(words[i], reting[i]);
        }
        return dictionary;
    }


    @BeforeEach
    public void setUp() {
        results = new ArrayList<>();

    }

    @AfterEach
    public void tearDown() {
        results = null;
    }

    @Test
    public void run() {
        //arrange
        final String[] words = new String[]{"hello", "hi", "bye", "goodbye"};
        final int[] retings = new int[]{5, 10, 10, 5};
        final String[] prefix = new String[]{"h", "he", "b", "some"};
        final String[][] expectedResultForPrefix = new String[][]{{"hi", "hello"},
                {"hello"}, {"bye"}, {}};

        dictionary = createDictionaryByArray(words, retings);
        prefixes = createPrefixesByArray(prefix);
        executor = new AutocompleteWordsExecutor(dictionary,  prefixes, ()-> true,
                results::add);
        final int resultSizeExpected = prefixes.size();

        //act
        executor.run();
        //assert
        assertEquals(resultSizeExpected, results.size());
        for (int i = 0; i < resultSizeExpected; i++) {
            List<String> resultForSinglePrefix = results.get(i);
            String[] expectedResult = expectedResultForPrefix[i];
            assertEquals(expectedResult.length, resultForSinglePrefix.size(),
                    "do not match the expected size of the completion list with the result " +
                            "for prefix \"" + prefix[i] + "\"");
            for (int j = 0; j < expectedResult.length ; j++) {
                assertEquals(expectedResult[j], resultForSinglePrefix.get(j), "Don't match" +
                        " lists completion for the prefix" + prefix[i] + "\"");
            }
        }
    }

}