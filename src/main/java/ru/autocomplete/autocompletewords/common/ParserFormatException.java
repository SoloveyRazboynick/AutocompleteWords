package ru.autocomplete.autocompletewords.common;

/**
 * Исключительние выбрасываемое при несоответствии входных данных заданному в парсере шаблону
 */
public class ParserFormatException extends  Exception {
    public ParserFormatException() {
        super();
    }

    public ParserFormatException(String message) {
        super(message);
    }

    public ParserFormatException(String message, Throwable cause) {
        super(message, cause);
    }

    public ParserFormatException(Throwable cause) {
        super(cause);
    }

    protected ParserFormatException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
