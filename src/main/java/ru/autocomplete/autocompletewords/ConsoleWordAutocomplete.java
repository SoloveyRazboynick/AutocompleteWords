package ru.autocomplete.autocompletewords;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ru.autocomplete.autocompletewords.models.RatingDictionary;
import ru.autocomplete.autocompletewords.services.AutocompleteWordsExecutor;
import ru.autocomplete.autocompletewords.services.Parser;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Точка входа в приложение
 */
public class ConsoleWordAutocomplete {
    private static final Logger LOGGER = LogManager.getLogger(ConsoleWordAutocomplete.class);

    private static void showResult(List<String> result){
        if (result.size()>0) {
            result.stream().forEach(System.out::println);
        }
        System.out.println();
    }

    /**
     * обрабатывает один файл (fi) формата *.in (описание формата см. в ТЗ)
     * для каждого префикса слова, указанного в fi, выводит список автодополнений в консоль
     * @param args - первым и единственным аргументом должен быть путь к файлу *,in
     */
    public static void main(String[] args) {
        if (args.length > 0) {
            try (FileInputStream fileIn = new FileInputStream(args[ 0 ])) {

                RatingDictionary ratingDictionary = new RatingDictionary();
                ArrayList<String> prefixes = new ArrayList<>();
                Parser parser = new Parser(ratingDictionary, prefixes, fileIn);
                Thread parserThread = new Thread(parser);
                parserThread.start();


                AutocompleteWordsExecutor autocompleteWordsExecutor = new
                        AutocompleteWordsExecutor(ratingDictionary, prefixes,
                        parser::isParsingFullComplited, ConsoleWordAutocomplete::showResult);
                Thread threadAutocomplete = new Thread(autocompleteWordsExecutor);
                threadAutocomplete.start();

                parserThread.join();
                threadAutocomplete.join();

            } catch (IOException | InterruptedException  e) {
                LOGGER.error(e);
            }
        } else {
            LOGGER.error("В качестве первого параметра программы должен быть указан путь к " +
                    "файлу \"*.in\" соответствующего формата");
        }
    }
}
