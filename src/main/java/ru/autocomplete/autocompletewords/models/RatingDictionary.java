package ru.autocomplete.autocompletewords.models;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Класс для хранения и обработки слов с их частотой употребляемости в речи
 */
public class RatingDictionary {

    /** Максимальное количество слов (для автозавершения) выдаваемое по префиксу  */
    private static final int MAX_WORD_COUNT_BY_PREFIX = 10;
    /** Количество букв в латинском алфавите */
    private static final int ALPHABET_SYMBOL_COUNT = 26;
    /** Количество букв в слове словаря */
    private static final int MAX_WORD_LENGTH = 15;
    /** Код символа, который меньше первой буквы алфавита */
    private static final String LESS_FIRST_SYMBOL = "\u0060";
    /** Код символа, который больше первой буквы алфавита */
    private static final String MORE_LAST_SYMBOL = "\u007B";
    /** Ожидаемая размерность словаря */
    private static final int EXPECTED_CAPACITY = 100_000;

    /** Альтернатива индексу: по 0 индуксу располагается set, в котором все слова начинаются c a */
    private ArrayList<TreeSet<String>> indexingByFirstSymbol = new ArrayList<>(ALPHABET_SYMBOL_COUNT);
    /** Для более быстрого получения количества повторений слова */
    private HashMap<String, Integer> hashDictionary = new HashMap<>(EXPECTED_CAPACITY);

    public RatingDictionary() {
        for (int i = 0; i < ALPHABET_SYMBOL_COUNT; i++) {
            indexingByFirstSymbol.add(new TreeSet<>());
        }
    }

    /**
     *  Добавляет слово и его количество повторений в словарь
     * @param word - слово
     * @param reting - количество повторений слова word в текстах(рейтинг слова)
     */
    public void addWord(String word, int reting){
        int bucketIndex = word.codePointAt(0) - 'a';
        indexingByFirstSymbol.get(bucketIndex).add(word);
        hashDictionary.put(word, reting);
    }

    /**
     * Поиск слов-автодополнений в словаре по первым буквам слова
     * @param
     * @return - по префиксу возвращает список из MAX_WORD_COUNT_BY_PREFIX самых популярных
     * вариантов дополнения отсортированных по частоте употребления слова в текстах, если в
     * словаре не найдется ни одного дополнения, вернет пустой список
     */
    public List<String> getAutocompleteListByPrefix(String prefix) {
        int index = prefix.codePointAt(0) - 'a';
        Set<String> subSet = indexingByFirstSymbol.get(index).subSet(prefix.concat(LESS_FIRST_SYMBOL),
                prefix.concat(MORE_LAST_SYMBOL));
        /* TreeMap с сортировкой в обратном порядке */
        TreeMap<Integer,String> temp = new TreeMap<>((o1, o2) -> (o1 - o2) * -1);
        for (String word :
                subSet) {
            temp.put(hashDictionary.get(word) , word);
        }
        return temp.values().stream().limit(MAX_WORD_COUNT_BY_PREFIX).collect(Collectors.toList());
    }

    /**
     * Количество слов в словаре
     * @return
     */
    public int getWordCount() {
        return hashDictionary.size();
    }

    public HashMap<String, Integer> getHashDictionary() {
        return hashDictionary;
    }
}
