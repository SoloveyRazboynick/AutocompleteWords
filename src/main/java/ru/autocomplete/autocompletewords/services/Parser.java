package ru.autocomplete.autocompletewords.services;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ru.autocomplete.autocompletewords.common.ParserFormatException;
import ru.autocomplete.autocompletewords.models.RatingDictionary;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Парсер входного потока следующего формата:
 * В первой строке находится единственное число N (1 ≤ N ≤ 10^5) - количество слов в словаре.
 * Каждая из следующих N строк содержит слово wi (непустая последовательность строчных латинских
 * букв длиной не более 15) и целое число ni (1 ≤ ni ≤ 10^6) - частота употребления слова wi.
 * Слово и число разделены единственным пробелом. Ни одно слово не повторяется более одного раза.
 * В (N+2)-й строке находится число M (1 ≤ M ≤ 15000). В следующих M строках содержатся слова uj
 * (непустая последовательность строчных латинских букв длиной не более 15) - начала слов,
 * введённых пользователем.
 */
public class Parser implements Runnable {
    private static final Logger LOGGER = LogManager.getLogger(Parser.class);
    /** шаблон для верификации слова во входном потоке на соответствие треабованиям
     *  - только латинские буквы в нижнем регистре
     *  - длина слова от 1 до 15
     */
    private static final String WORD_PATTERN = "[a-z]{1,15}";

    /** Флаг завершения разбора входного потока */
    private volatile boolean parsingFullComplited;
    /** Флаг завершения разбора части словаря в потоке */
    private volatile boolean parsingDictionaryComplited;
    /** Флаг завершения разбора части префиксов в потоке */
    private volatile boolean parsingPrefixComplited;

    /** хранит исключение, возникщее во время разбора потока */
    private Exception exception;

    private RatingDictionary ratingDictionary;
    private ArrayList<String> prefix;
    private InputStream io;

    public Parser(RatingDictionary ratingDictionary, ArrayList<String> prefix, InputStream io) {
        this.ratingDictionary = ratingDictionary;
        this.prefix = prefix;
        this.io = io;
    }

    private void parseRatingDictionary(Scanner scanner, RatingDictionary dictionary)
            throws ParserFormatException {
        try {
            int wordCount = 0;
            if (scanner.hasNext()) {
                wordCount = scanner.nextInt();
            }

            String singleWord;
            int repeatCount;
            for (int i = 0; i < wordCount; i++) {
                if (!scanner.hasNext(WORD_PATTERN)) {
                    throw new ParserFormatException("Очередное слво в словаре не соответствует " +
                            "заданному формату");
                }
                singleWord = scanner.next();

                if (!scanner.hasNextInt()) {
                    throw new ParserFormatException("Для слова в словаре не указана частот его " +
                            "использования в речи");
                }
                repeatCount = scanner.nextInt();

                dictionary.addWord(singleWord, repeatCount);
            }
        }catch (Exception e){
            LOGGER.error(e);
            throw e;
        }
    }

    private void parsePrefix(Scanner scanner, List<String> wordPrefixes)
            throws ParserFormatException {
        try {
            int prefixCount;
            if (!scanner.hasNextInt()) {
                throw new ParserFormatException();
            }
            prefixCount = scanner.nextInt();

            for (int i = 0; i < prefixCount; i++) {
                if (!scanner.hasNext(WORD_PATTERN)) {
                    throw new ParserFormatException();
                }
                wordPrefixes.add(scanner.next());
            }
        }catch (ParserFormatException e){
            LOGGER.error(e);
            throw e;
        }
    }

    /**
     * Метод парсинга входного потока следующего формата:
     * В первой строке находится единственное число N (1 ≤ N ≤ 10^5) - количество слов в словаре.
     * Каждая из следующих N строк содержит слово wi (непустая последовательность строчных латинских
     * букв длиной не более 15) и целое число ni (1 ≤ ni ≤ 10^6) - частота употребления слова wi.
     * Слово и число разделены единственным пробелом. Ни одно слово не повторяется более одного раза.
     * В (N+2)-й строке находится число M (1 ≤ M ≤ 15000). В следующих M строках содержатся слова uj
     * (непустая последовательность строчных латинских букв длиной не более 15) - начала слов,
     * введённых пользователем.
     * входного потока формату, описанному выше
     */
    @Override
    public void run() {
        exception = null;
        parsingFullComplited = parsingDictionaryComplited = parsingPrefixComplited = false;
        try (Scanner scanner = new Scanner(io) ) {
            parseRatingDictionary(scanner, ratingDictionary);
            parsingDictionaryComplited = true;
            parsePrefix(scanner, prefix);
            parsingPrefixComplited = true;
            parsingFullComplited = true;
        }catch (ParserFormatException e){
            LOGGER.error(e);
            exception = e;
        }
    }

    public boolean isParsingFullComplited(){
        return parsingFullComplited;
    }

    public boolean isParsingDictionaryComplited() {
        return parsingDictionaryComplited;
    }

    public boolean isParsingPrefixComplited() {
        return parsingPrefixComplited;
    }

    public boolean isComplitedWithException() {
        return (exception!= null);
    }

    public Exception getException() {
        return exception;
    }
}
