package ru.autocomplete.autocompletewords.services;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ru.autocomplete.autocompletewords.models.RatingDictionary;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;
import java.util.function.Consumer;
import java.util.function.Supplier;

/**
 * Класс отвечает за поиск дополнений слова по его префиксу, обрабатывая каждый преикс в
 * отдельном потоке
 */
public class AutocompleteWordsExecutor implements Runnable {
    /**
     * Класс отвечает за обработку одного префикса - поиск списка дополнений слова
     * необходим для обработки каждого префикса в отдельном потоке
     */
    class Autocomplete implements Callable<List<String>> {
        private RatingDictionary ratingDictionary;
        private String prefix;

        public Autocomplete(RatingDictionary ratingDictionary, String prefix) {
            this.ratingDictionary = ratingDictionary;
            this.prefix = prefix;
        }

        /**
         * Поиск списка слов-дополнений по префиксу
         * @return - список автодополнений
         * @throws Exception
         */
        @Override
        public List<String> call() throws Exception {
            return ratingDictionary.getAutocompleteListByPrefix(prefix);
        }
    }

    private static final Logger LOGGER = LogManager.getLogger(AutocompleteWordsExecutor.class);

    private RatingDictionary dictionary;
    private ArrayList<String> prefixes;
    private Supplier<Boolean> allPrefixAdded;
    private Consumer<List<String>> consumer;

    /**
     * @param dictionary - заполненный словарь на основе частоты употребления слов
     * @param prefixes - список префиксов, для которых необходимо получить список возможных
     *                 дополнений
     * @param allPrefixAdded - интерфейс, метод которого должен возвращать флаг того, что все
     *                       префиксы добавлены в список prefixes на этапе запуска
     * @param consumer - интерфес, метод которого отвечает за приём результата обработки префикса
     */
    public AutocompleteWordsExecutor(RatingDictionary dictionary, ArrayList<String> prefixes,
                                     Supplier<Boolean> allPrefixAdded, Consumer<List<String>> consumer) {
        this.dictionary = dictionary;
        this.prefixes = prefixes;
        this.allPrefixAdded = allPrefixAdded;
        this.consumer = consumer;
    }

    /**
     * в многопоточном режиме запускает поиск вариантов дополнения для всех prefixes, для каждого
     * префикса результат  возвращается путем его передачи через интерфейс consumer. Результат
     * возвращается в порядке расположения префиксов, т.е для первого префикса результат вернется
     * первым по принципу FIFO
     * Выполнение будет продолжаться до тех пор пока все префиксы не будут обработаны и метод
     * интерфейса allPrefixAdded не вернет флаг установленный в значение true, означающий, что
     * все необходимые префиксы были добавлены в список.
     *
     * Результат обработки префиксов будет возвращен только после обработки всех префиксов
     */
    public void run(){
        ExecutorService poolForPrefix = Executors.newCachedThreadPool();
        ArrayList<Future<List<String>>> results = new ArrayList<>(prefixes.size());
        int currentPrefixCount = 0;
        while ( (!allPrefixAdded.get()) || (currentPrefixCount < prefixes.size())){
            if (currentPrefixCount >= prefixes.size()) {
                continue;
            }
            Autocomplete autocomplete = new Autocomplete(dictionary, prefixes.get(currentPrefixCount));
            results.add(poolForPrefix.submit(autocomplete));
            currentPrefixCount++;

        }

        poolForPrefix.shutdown();

        for(Future<List<String>> feature :
                results){
            try {
                consumer.accept(feature.get());
            } catch (InterruptedException | ExecutionException e) {
                LOGGER.error(e);
            }
        }
    }
}
